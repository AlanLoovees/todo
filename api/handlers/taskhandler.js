var express = require('express')
const task = require('../models/task')
// const user = require('../models/user')

var router = express.Router()

// router.post('/signup', function(req, res){
//     user.create({username:req.body.username}, function(err, user){
//         if(err){
//             return res.send(err)
//         }
//         return res.json({user:user})
//     })
// })

// router.post('/signin', function(req, res){
//     user.find({}, function(err, user){
//         if(err){
//             return res.send(err)
//         }
//         return res.json({user:user})
//     })
// })

router.post('/add', function(req, res){
    task.create({description:req.body.description},
        function(err, tasks){
            if(err){
                return res.send(err)
            }
            return res.json({tasks:tasks})
        })
})

router.get('/get', function(req, res){
    task.find({}, function(err, tasks){
        if(err){
            return res.send(err)
        }
        return res.json({tasks:tasks})
    })
})

router.delete('/delete', function(req, res){
    task.findByIdAndDelete(req.body._id, function(err, tasks){
        if(err){
            return res.send(err)
        }
        return res.json({tasks:tasks})
    })
})

module.exports = router